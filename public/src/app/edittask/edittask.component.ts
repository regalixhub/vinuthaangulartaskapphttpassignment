import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-edittask',
  templateUrl: './edittask.component.html',
  styleUrls: ['./edittask.component.css']
})
export class EdittaskComponent implements OnInit {
  private task: Task;
  constructor(  private tasks: TasksService,
                private router: Router,
                private route: ActivatedRoute) { }

  ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
      const  index = parseInt(params.get('id'), 10);
      this.tasks.getTaskById(index).subscribe(task => this.task = task);
    });
  }

 update() {
   this.tasks.editTask(this.task).subscribe(() =>
   this.router.navigate(['/list']));
  }

}
