import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddtaskComponent } from './addtask/addtask.component';
import { ListtaskComponent } from './listtask/listtask.component';
import { EdittaskComponent } from './edittask/edittask.component';
import { DetailtaskComponent } from './detailtask/detailtask.component';

const routes: Routes = [
  {path: '', component: ListtaskComponent},
  {path: 'add', component: AddtaskComponent},
  {path: 'list', component: ListtaskComponent},
  {path: 'edit/:id', component: EdittaskComponent},
  {path: 'detail/:id', component: DetailtaskComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
