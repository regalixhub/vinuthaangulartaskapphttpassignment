import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from './task';

@Injectable({
  providedIn: 'root'
})

export class TasksService {

  private baseUrl = 'http://localhost:8080/api/task/';
  private headers = new HttpHeaders({'Content-type': 'application/json'});
  private options = {headers : this.headers};

  constructor(private http: HttpClient) { }
  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.baseUrl + '/', this.options);
  }

 addTask(data): Observable<Task> {
  return this.http.post<Task>(this.baseUrl, JSON.stringify(data), this.options);
  }

 getTaskById(id: number): Observable<Task> {
   return this.http.get<Task>(this.baseUrl + '/' + id, this.options);
  }

 deleteTask(id: number) {
   return this.http.delete(this.baseUrl + id, this.options);
  }

  editTask(task: Task): Observable<Task> {
    return this.http.put<Task>(this.baseUrl + '/' + task.id, JSON.stringify(task), this.options);
  }

}
