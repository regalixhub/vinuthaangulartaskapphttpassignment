import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddtaskComponent } from './addtask/addtask.component';
import { ListtaskComponent } from './listtask/listtask.component';
import { TasksService } from './tasks.service';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { EdittaskComponent } from './edittask/edittask.component';
import { DetailtaskComponent } from './detailtask/detailtask.component';

@NgModule({
  declarations: [
    AppComponent,
    AddtaskComponent,
    ListtaskComponent,
    EdittaskComponent,
    DetailtaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TasksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
